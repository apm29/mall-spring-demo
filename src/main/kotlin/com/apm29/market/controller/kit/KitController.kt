package com.apm29.market.controller.kit

import com.apm29.market.db.mapper.KitTypeMapper
import com.apm29.market.db.entity.KitType
import com.apm29.market.db.mapper.KitMapperPlus
import com.apm29.market.db.mapper.KitTypeMapperPlus
import com.apm29.market.domain.bean.BaseResponse
import com.baomidou.mybatisplus.extension.kotlin.KtQueryWrapper
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.transaction.annotation.Transactional
import org.springframework.validation.annotation.Validated
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController

@RestController
class KitController {
    @Autowired
    lateinit var kitTypeMapperPlus: KitTypeMapperPlus

    @Autowired
    lateinit var kitMapperPlus: KitMapperPlus

    @Autowired
    lateinit var kitTypeMapper: KitTypeMapper


    @PostMapping("/kitType/get")
    fun getAllKitType(): BaseResponse<List<KitType>> {
        return BaseResponse.ok(
                kitTypeMapperPlus.selectList(
                        KtQueryWrapper(KitType::class.java)
                )
        )
    }

    @PostMapping("/kitType/getAsTree")
    fun getAllKitTypeAsTree(): BaseResponse<List<KitType>> {
        return BaseResponse.ok(
                kitTypeMapper.getNodeTree()
        )
    }


    @PostMapping("/kitType/add")
    fun addNewKitType(
            @Validated kitType: KitType
    ): BaseResponse<Any> {
        kitTypeMapperPlus.insert(kitType)
        return BaseResponse.ok()
    }

    @PostMapping("/kitType/delete")
    @Transactional
    fun deleteKitType(
            @RequestParam id:Int
    ): BaseResponse<Any> {
        val kitType:KitType? = kitTypeMapperPlus.selectById(id)
        return if(kitType!=null) {
            if (kitType.parentTypeId == null || kitType.parentTypeId == 0) {
                val subTypes = kitTypeMapperPlus.selectList(
                        KtQueryWrapper(KitType::class.java)
                                .eq(KitType::parentTypeId, kitType.id)
                )
                kitTypeMapperPlus.deleteBatchIds(subTypes.map { it.id })
            }
            kitTypeMapperPlus.deleteById(id)
            BaseResponse.ok()
        }else{
            BaseResponse.error(message = "该类型不存在")
        }
    }


}