package com.apm29.market.db.mapper

import com.apm29.market.db.entity.Kit
import com.baomidou.mybatisplus.core.mapper.BaseMapper

interface KitMapperPlus:BaseMapper<Kit>