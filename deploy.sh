#!/bin/bash

kill `netstat -nlp | grep :9998 | awk '{print $7}' | awk -F"/" '{ print $1 }'`
clear
nohup java -Dfile.encoding=utf-8 -jar demo-0.0.1.jar>nohup.log &