package com.apm29.market.db.mapper

import com.apm29.market.db.entity.KitType
import org.apache.ibatis.annotations.Mapper

@Mapper
interface KitTypeMapper {
    fun getNodeTree():List<KitType>

    fun getNextNodeTree(id:Int):List<KitType>
}
