package com.apm29.market.db.mapper

import com.apm29.market.db.entity.UserEntity
import com.baomidou.mybatisplus.core.mapper.BaseMapper

interface UserMapperPlus : BaseMapper<UserEntity>