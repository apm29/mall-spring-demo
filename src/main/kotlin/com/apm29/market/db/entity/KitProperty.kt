package com.apm29.market.db.entity

import com.baomidou.mybatisplus.annotation.IdType
import com.baomidou.mybatisplus.annotation.TableId
import com.baomidou.mybatisplus.annotation.TableName
import io.swagger.annotations.ApiModel
import io.swagger.annotations.ApiModelProperty

@TableName("t_kit_property")
@ApiModel("配件属性表")
data class KitProperty(
        @TableId(type = IdType.AUTO)
        @ApiModelProperty("配件属性ID")
        val id:Int? = 0,

        @ApiModelProperty("属性名称")
        val name: String,

        @ApiModelProperty("类型")
        val type: Int
)