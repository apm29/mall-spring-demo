package com.apm29.market.db.entity

import com.baomidou.mybatisplus.annotation.IdType
import com.baomidou.mybatisplus.annotation.TableField
import com.baomidou.mybatisplus.annotation.TableId
import com.baomidou.mybatisplus.annotation.TableName
import io.swagger.annotations.ApiModel
import io.swagger.annotations.ApiModelProperty

@TableName("t_kit")
@ApiModel("配件表")
data class Kit(
        @TableId(type = IdType.AUTO)
        @ApiModelProperty("配件ID")
        var id: Int? = 0,

        @ApiModelProperty("配件名称")
        val name: String,

        @ApiModelProperty("配件价格")
        val price: Double? = 0.0,

        @ApiModelProperty("配件价格单位 例如/g")
        @TableField("price_unit")
        val priceUnit: String? = "个",

        @ApiModelProperty("配件数量")
        @TableField("kit_count")
        val count: Int? = 0,

        @ApiModelProperty("配件属性:JSONArray")
        @TableField("properties")
        val properties: String? = "[]",


        @ApiModelProperty("类型")
        val type: Int
)