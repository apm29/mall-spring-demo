package com.apm29.market.db.entity

import com.baomidou.mybatisplus.annotation.IdType
import com.baomidou.mybatisplus.annotation.TableField
import com.baomidou.mybatisplus.annotation.TableId
import com.baomidou.mybatisplus.annotation.TableName
import io.swagger.annotations.ApiModel
import io.swagger.annotations.ApiModelProperty
import javax.validation.constraints.NotEmpty
import javax.validation.constraints.NotNull

@TableName("t_kit_type")
@ApiModel("配件属性表")
data class KitType @JvmOverloads constructor(
        @TableId(type = IdType.AUTO)
        @ApiModelProperty("配件类型ID")
        val id: Int? = 0,

        @field:NotNull(message = "父类型不能为空")
        @ApiModelProperty("配件父类型ID")
        @TableField("parent_type_id")
        val parentTypeId: Int?,

        @field:NotEmpty(message = "类型名称不能为空")
        @ApiModelProperty("类型名称")
        val name: String?,

        @Transient
        var children:List<KitType>? = arrayListOf()
)