package com.apm29.market.db.mapper

import com.apm29.market.db.entity.KitType
import com.baomidou.mybatisplus.core.mapper.BaseMapper

interface KitTypeMapperPlus:BaseMapper<KitType>