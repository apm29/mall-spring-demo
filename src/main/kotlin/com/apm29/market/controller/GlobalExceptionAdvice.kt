package com.apm29.market.controller

import com.apm29.market.controller.exception.ServiceException
import com.apm29.market.domain.bean.BaseResponse
import org.springframework.http.HttpStatus
import org.springframework.http.converter.HttpMessageNotReadableException
import org.springframework.validation.BindException
import org.springframework.web.HttpMediaTypeNotSupportedException
import org.springframework.web.HttpRequestMethodNotSupportedException
import org.springframework.web.bind.MethodArgumentNotValidException
import org.springframework.web.bind.MissingServletRequestParameterException
import org.springframework.web.bind.annotation.ExceptionHandler
import org.springframework.web.bind.annotation.ResponseStatus
import org.springframework.web.bind.annotation.RestControllerAdvice
import org.springframework.web.servlet.NoHandlerFoundException
import javax.validation.ConstraintViolationException
import javax.validation.ValidationException


@RestControllerAdvice
class GlobalExceptionAdvice {

    /**
     * 业务异常
     */
    @ResponseStatus(HttpStatus.INTERNAL_SERVER_ERROR)
    @ExceptionHandler(ServiceException::class)
    fun handlerServiceException(e : ServiceException) : BaseResponse<Any>{
        return BaseResponse.error(e.code, e.message?:"未知异常")
    }

    /**
     * 400 -Bad Request
     */
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MissingServletRequestParameterException::class)
    fun handleMissingServletRequestParameterException(e: MissingServletRequestParameterException): BaseResponse<Any> {

        return BaseResponse.error(400, "缺少请求参数${e.parameterName},类型:${e.parameterType}")
    }

    /**
     * 400 -Bad Request
     */
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(HttpMessageNotReadableException::class)
    fun handleHttpMessageNotReadableException(e: HttpMessageNotReadableException): BaseResponse<Any> {
        return BaseResponse.error(400, "参数解析失败")
    }

    /**
     * 400 - Bad Request
     */
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(MethodArgumentNotValidException::class)
    fun handleMethodArgumentNotValidException(e: MethodArgumentNotValidException): BaseResponse<Any> {
        val result = e.bindingResult
        val error = result.fieldError
        val field = error?.field
        val code = error?.defaultMessage
        val message = String.format("参数验证失败(%s):%s", field, code)
        return BaseResponse.error(400, message)
    }

    /**
     * 400 - Bad Request
     */
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(BindException::class)
    fun handleBindException(e: BindException): BaseResponse<Any> {
        val result = e.bindingResult
        val error = result.fieldError
        val field = error?.field
        val code = error?.defaultMessage
        val message = String.format("参数绑定失败(%s):%s", field, code)
        return BaseResponse.error(400, message)
    }

    /**
     * 400 - Bad Request
     */
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(ConstraintViolationException::class)
    fun handleConstraintViolationException(e: ConstraintViolationException): BaseResponse<Any> {
        val violations = e.constraintViolations
        val violation = violations.iterator().next()
        val message = "参数验证失败" + violation.message
        return BaseResponse.error(400, message)
    }

    /**
     * 400 - Bad Request
     */
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(ValidationException::class)
    fun handleValidationException(e: ValidationException): BaseResponse<Any> {
        return BaseResponse.error(400, "参数验证失败")
    }

    /**
     * 400 - Bad Request
     */
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ExceptionHandler(NumberFormatException::class)
    fun handleNumberFormatException(e: NumberFormatException): BaseResponse<Any> {
        val message = "参数转换失败:" + e.message
        return BaseResponse.error(400, message)
    }

    /**
     * 404 - Not Found
     */
    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ExceptionHandler(NoHandlerFoundException::class)
    fun noHandlerFoundException(e: NoHandlerFoundException): BaseResponse<Any> {
        return BaseResponse.error(404, "Not Found" + e)
    }

    /**
     * 405 - Method Not Allowed
     */
    @ResponseStatus(HttpStatus.METHOD_NOT_ALLOWED)
    @ExceptionHandler(HttpRequestMethodNotSupportedException::class)
    fun handleHttpRequestMethodNotSupportedException(e: HttpRequestMethodNotSupportedException): BaseResponse<Any> {
        return BaseResponse.error(405, "不支持当前请求方法")
    }

    /**
     * 415 - Unsupported Media Type
     */
    @ResponseStatus(HttpStatus.UNSUPPORTED_MEDIA_TYPE)
    @ExceptionHandler(HttpMediaTypeNotSupportedException::class)
    fun handleHttpMediaTypeNotSupportedException(e: HttpMediaTypeNotSupportedException): BaseResponse<Any> {
        return BaseResponse.error(415, "不支持当前媒体类型")
    }


}