package com.apm29.market.db.entity

import java.time.LocalDateTime

data class MallTestEntity(
        val name:String,
        var updateTime:LocalDateTime,
        var deleted:Int
)