package com.apm29.market.controller

import com.apm29.market.db.entity.KitType
import com.apm29.market.db.entity.MallTestEntity
import com.apm29.market.db.entity.UserEntity
import com.apm29.market.db.mapper.KitMapperPlus
import com.apm29.market.db.mapper.KitTypeMapperPlus
import com.apm29.market.db.mapper.UserMapper
import com.apm29.market.db.mapper.UserMapperPlus
import com.apm29.market.domain.bean.BaseResponse
import com.baomidou.mybatisplus.extension.kotlin.KtQueryWrapper
import com.baomidou.mybatisplus.extension.plugins.pagination.Page
import io.swagger.annotations.Api
import io.swagger.annotations.ApiOperation
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.validation.annotation.Validated
import org.springframework.web.bind.annotation.GetMapping
import org.springframework.web.bind.annotation.PostMapping
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.bind.annotation.RestController
import java.time.LocalDateTime
import javax.annotation.Resource


@RestController
@Validated
@Api("测试相关")
class Greet {

    @Resource
    lateinit var userMapper: UserMapper

    @Autowired
    lateinit var userMapperPlus: UserMapperPlus

    @Autowired
    lateinit var kitMapperPlus: KitMapperPlus

    @Autowired
    lateinit var kitTypeMapperPlus: KitTypeMapperPlus

    @GetMapping("/hello")
    @ApiOperation("Echo Hello")
    fun hello(
            @RequestParam(
                    value = "name", defaultValue = "World"
            ) name: String
    ): BaseResponse<String> {
        return BaseResponse.ok("hello  $name!")
    }

    @PostMapping("/person")
    fun person(
            @RequestParam(
                    value = "page", defaultValue = "1"
            ) page: Long,
            @RequestParam(
                    value = "size", defaultValue = "20"
            ) size: Long
    ): BaseResponse<Any> {

        return BaseResponse.ok(
                userMapperPlus.selectPage<Page<UserEntity>>(Page(
                        page, size
                ), null)
        )
    }

    @PostMapping("/test")
    fun test(
            @RequestParam(
                    value = "name"
            ) name: String
    ): BaseResponse<Any> {
        userMapper.insertTestTable(MallTestEntity(
                name, LocalDateTime.now(), 0
        ))
        return BaseResponse.ok(

        )
    }

    @PostMapping("/add")
    fun addUser(
            @Validated userEntity: UserEntity
    ): BaseResponse<UserEntity> {
        val count = userMapperPlus.insert(userEntity)
        println(count)
        return BaseResponse.ok()
    }

}





