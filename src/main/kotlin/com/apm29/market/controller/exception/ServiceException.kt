package com.apm29.market.controller.exception

/**
 * 业务用 异常
 */
class ServiceException(val code : Int, message : String) : RuntimeException(message) {

}