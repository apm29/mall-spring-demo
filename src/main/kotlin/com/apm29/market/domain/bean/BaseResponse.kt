package com.apm29.market.domain.bean

import com.fasterxml.jackson.annotation.JsonProperty

data class BaseResponse<T> constructor(
        @get:JsonProperty("Code") val Code: Int,
        @get:JsonProperty("Msg") val Msg: String,
        @get:JsonProperty("Data") val Data: T?
) {
    companion object {
        fun <T> ok(data: T? = null): BaseResponse<T> {
            return BaseResponse(200, "Ok", data)
        }

        fun error(code: Int = 500, message: String?): BaseError {
            return BaseResponse(code, message ?: "未知错误", null)
        }
    }
}

typealias BaseError = BaseResponse<Any>

