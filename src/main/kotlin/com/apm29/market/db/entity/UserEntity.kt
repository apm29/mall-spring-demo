package com.apm29.market.db.entity

import com.baomidou.mybatisplus.annotation.TableField
import com.baomidou.mybatisplus.annotation.TableName
import java.time.LocalDate
import java.time.LocalDateTime
import javax.validation.constraints.NotNull
import javax.validation.constraints.Size

@TableName("litemall_user")
data class UserEntity(
        var id: Int?=0,
        @field:NotNull
        @field:Size(min = 2, max = 30,message = "名字要求最大30个字符,最小2个字符")
        @TableField("username") var userName: String,
        @field:NotNull
        @field:Size(min = 2, max = 30,message = "密码最大20个字符,最小6个字符")
        val password: String,
        val gender: Int?,
        val birthday: LocalDate?,
        @TableField("last_login_time") val lastLoginTime: LocalDateTime?,
        @TableField("last_login_ip") val lastLoginIp: String?,
        @TableField("user_level") val userLevel: Int?,
        @TableField("nickname") val nickName: String?,
        val mobile: String?,
        val avatar: String?,
        @TableField("weixin_openid") val weixinOpenId: String?,
        @TableField("session_key") val sessionKey: String?,
        val status: Int?,
        @TableField("add_time") val addTime: LocalDateTime?,
        @TableField("update_time") val updateTime: LocalDateTime?,
        val deleted: Int?
)