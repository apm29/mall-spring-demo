package com.apm29.market.config

import org.springframework.context.annotation.Configuration
import springfox.documentation.builders.ApiInfoBuilder
import springfox.documentation.builders.PathSelectors
import springfox.documentation.builders.RequestHandlerSelectors
import springfox.documentation.service.ApiInfo
import springfox.documentation.service.Contact
import springfox.documentation.spi.DocumentationType
import springfox.documentation.spring.web.plugins.Docket
import springfox.documentation.swagger2.annotations.EnableSwagger2

@Configuration
@EnableSwagger2
class SwaggerConfig {

    fun createRestfulApi(): Docket {
        return Docket(DocumentationType.SWAGGER_2)
                .pathMapping("/")
                .select()
                .apis(RequestHandlerSelectors.basePackage("com.apm29.market"))
                .paths(PathSelectors.any())
                .build()
                .apiInfo(
                        ApiInfoBuilder()
                                .title("Market Swagger")
                                .description("佳谕Mall")
                                .version("1.0.0")
                                .contact(Contact("apm29","apm9.github.com","yjw999wow@gmail.com"))
                                .build()
                )
    }
}