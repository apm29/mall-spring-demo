package com.apm29.market.db.mapper

import com.apm29.market.db.entity.MallTestEntity
import com.apm29.market.db.entity.UserEntity
import org.apache.ibatis.annotations.Insert
import org.apache.ibatis.annotations.Mapper
import org.apache.ibatis.annotations.Param
import org.apache.ibatis.annotations.Select

@Mapper
interface UserMapper {

    @Select("SELECT * FROM litemall_user WHERE username = #{userName}")
    fun findByUserName(@Param("userName") userName:String):List<UserEntity>

    // @Insert("insert into cluster_manager(cluster_name, cluster_time, cluster_address, cluster_access_user, cluster_access_passwd) " +
    //            "values(#{clusterName}, #{clusterTime}, #{clusterAddress}, #{clusterAccessUser}, #{clusterAccessPasswd})")
    @Insert("INSERT INTO mall_test(name,update_time,deleted) VALUES(#{name},#{updateTime},#{deleted})")
    fun insertTestTable(test: MallTestEntity)
}