import org.jetbrains.kotlin.gradle.tasks.KotlinCompile
buildscript{
	repositories {
		maven ("https://plugins.gradle.org/m2/")
		maven ("https://maven.aliyun.com/repository/gradle-plugin")
		maven ("https://maven.aliyun.com/repository/google")
		maven ("http://maven.aliyun.com/nexus/content/groups/public/")
		maven ("https://maven.aliyun.com/repository/jcenter")
	}
}

plugins {
	id("org.springframework.boot") version "2.2.5.RELEASE"
	id("io.spring.dependency-management") version "1.0.9.RELEASE"
	kotlin("jvm") version "1.3.61"
	kotlin("plugin.spring") version "1.3.61"
}

group = "com.apm29.market"
version = "0.0.1"
java.sourceCompatibility = JavaVersion.VERSION_1_8

val developmentOnly: Configuration by configurations.creating
configurations {
	runtimeClasspath {
		extendsFrom(developmentOnly)
	}
	compileOnly {
		extendsFrom(configurations.annotationProcessor.get())
	}
}

repositories {
	maven ("https://maven.aliyun.com/repository/gradle-plugin")
	maven ("https://maven.aliyun.com/repository/google")
	maven ("http://maven.aliyun.com/nexus/content/groups/public/")
	maven ("https://maven.aliyun.com/repository/jcenter")

}

dependencies {
	implementation("org.springframework.boot:spring-boot-starter-web")
	implementation("org.springframework.boot:spring-boot-starter-logging")
	implementation("org.jetbrains.kotlin:kotlin-reflect")
	implementation("org.jetbrains.kotlin:kotlin-stdlib-jdk8")
	developmentOnly("org.springframework.boot:spring-boot-devtools")
	annotationProcessor("org.springframework.boot:spring-boot-configuration-processor")

	implementation("org.mybatis.spring.boot:mybatis-spring-boot-starter:2.1.2")
	implementation("com.baomidou:mybatis-plus-boot-starter:3.3.1")
	implementation("mysql:mysql-connector-java:5.1.44")
	testImplementation("org.springframework.boot:spring-boot-starter-test") {
		exclude(group = "org.junit.vintage", module = "junit-vintage-engine")
	}

	//swagger ui
	implementation("io.springfox:springfox-swagger2:2.9.2")
	implementation("io.springfox:springfox-swagger-ui:2.9.2")
}



tasks.withType<Test> {
	useJUnitPlatform()
}

tasks.withType<KotlinCompile> {
	kotlinOptions {
		freeCompilerArgs = listOf("-Xjsr305=strict")
		jvmTarget = "1.8"
	}
}
